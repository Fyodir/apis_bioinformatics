# apis_bioinformatics

A list of APIs for use within Bioinformatics

## National Centre for Biotechnology Information (NCBI)

<table>
    <tr>
        <th>API</th>
        <th>Description</th>
        <th>Link</th>
    </tr>
    <tr>
        <td>BLAST URL API</td>
        <td>Allows developers to submit BLAST searches for processing at NCBI or cloud service provider(s) using HTTPS</td>
        <td>`https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=DeveloperInfo`</td>
    </tr>
    <tr>
        <td>PubMed Central (PMC) APIs</td>
        <td>Several APIs that provide programmatic access to various services that deal with PMC literature content</td>
        <td>`https://www.ncbi.nlm.nih.gov/pmc/tools/developers/`</td>
    </tr>
</table> 

## HUGO Gene Nomenclature Committee (HGNC)

<table>
    <tr>
        <th>API</th>
        <th>Description</th>
        <th>Link</th>
    </tr>
    <tr>
        <td>REST web-service</td>
        <td>REST web-service is a convenient and quick way of searching and fetching data from the HGNC database</td>
        <td>`https://www.genenames.org/help/rest/`</td>
    </tr>
</table>

## Human Phenotype Ontology (HPO)

<table>
    <tr>
        <th>API</th>
        <th>Description</th>
        <th>Link</th>
    </tr>
    <tr>
        <td>HPO Api</td>
        <td>API for querying the HPO database for gene ontology information</td>
        <td>`https://hpo.jax.org/webjars/swagger-ui/3.20.9/index.html?url=/api/hpo/docs`</td>
    </tr>
</table>

Python3 Code example

```
def extractHpoTerms(GeneSymbol):
    "Extract HPO terms from HPO API into a dicitonary using a given Entrez gene symbol"

    entrezFromGene = "https://hpo.jax.org/api/hpo/search/?q={}" # gene symbol
    hpoFromEntrez ="https://hpo.jax.org/api/hpo/gene/{}" # entrez id

    data = json.load(urllib.request.urlopen(entrezFromGene.format(GeneSymbol)))

    ontology = {}
    if len(data["genes"]) == 0:
        return  False
    else:
        for entry in data["genes"]:
            if entry["entrezGeneSymbol"] == GeneSymbol:
                entrezId = entry["entrezGeneId"]
            else:
                return ontology

        data = json.load(urllib.request.urlopen(hpoFromEntrez.format(entrezId)))

        for entry in data["termAssoc"]:
            ontology[entry["name"]] = entry["ontologyId"]

        return ontology
```
